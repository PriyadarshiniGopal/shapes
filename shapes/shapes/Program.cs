﻿using System;

namespace Shapes
{
    //  Base class shape gives default dfinitionof area nd perimeter
    class Shape
    {
        /// <summary>
        /// Area of shape
        /// </summary>
        /// <returns></returns>
        public virtual double Area()
        {
            return 0;
        }

        /// <summary>
        /// Perimeter of shape
        /// </summary>
        /// <returns></returns>
        public virtual double Perimeter()
        {
            return 0;
        }
    }
    // Class for Square shape
    class Square : Shape
    {
        // side of the square
        private float side;
        /// <summary>
        /// Constructor of square to get side value
        /// </summary>
        /// <param name="sideValue">side of the square</param>
        public Square(float sideValue)
        {
            side = sideValue;
        }
        /// <summary>
        /// Area of square
        /// </summary>
        /// <returns>double value gives area of square</returns>
        public override double Area()
        {
            return side*side;
        }
        /// <summary>
        /// Perimeter of Square
        /// </summary>
        /// <returns>double value gives perimeter of square</returns>
        public override double Perimeter()
        {
            return 4*side;
        }
    }
    // Class for Square shape
    class Circle : Shape
    {
        // side of the square
        private float radius;
        /// <summary>
        /// Constructor of circle to get radius value
        /// </summary>
        /// <param name="radiusValue">Radius of circle</param>
        public Circle(float radiusValue)
        {
            radius = radiusValue;
        }
        /// <summary>
        /// Area of Circle
        /// </summary>
        /// <returns>double value gives area of Circle</returns>
        public override double Area()
        {
            return Math.PI * radius * radius;
        }
        /// <summary>
        /// Perimeter of Circle
        /// </summary>
        /// <returns>double value gives perimeter of Circle</returns>
        public override double Perimeter()
        {
            return 2 * Math.PI * radius;
        }
    }
    // Class for Square shape
    class Rectangle : Shape
    {
        // length and width of a rectangle
        private float length, width;
        /// <summary>
        /// Constructor of Rectangle to assign length and width
        /// </summary>
        /// <param name="lengthValue">Length of the Rectangle</param>
        /// <param name="widthValue">width of the Rectangle</param>
        public Rectangle(float lengthValue,float widthValue)
        {
            length = lengthValue;
            width = widthValue;
        }
        /// <summary>
        /// Area of Rectangle
        /// </summary>
        /// <returns>double value gives area of rectangle</returns>
        public override double Area()
        {
            return length * width;
        }
        /// <summary>
        /// Perimeter of Rectangle
        /// </summary>
        /// <returns>double value gives perimeter of Rectangle</returns>
        public override double Perimeter()
        {
            return 2 * length + 2 * width;
        }
    }
    // Class for Square shape
    class Triangle : Shape
    {
        // base,height and side values of Triangle
        private float baseValue,height,sideOne,sideTwo;
        /// <summary>
        /// Constructor to assign values of triangle
        /// </summary>
        /// <param name="basevalue">Base of triangle</param>
        /// <param name="heightValue">hight of triangle</param>
        /// <param name="side1">Side value of triangle</param>
        /// <param name="side2">side value of triangle</param>
        public Triangle(float basevalue, float heightValue, float side1, float side2)
        {
            baseValue = basevalue;
            height = heightValue;
            sideOne = side1;
            sideTwo = side2;
        }
        /// <summary>
        /// Area of Triangle
        /// </summary>
        /// <returns>double value gives area of Triangle</returns>
        public override double Area()
        {
            return baseValue * height / 2;
        }
        /// <summary>
        /// Perimeter of Triangle
        /// </summary>
        /// <returns>double value gives perimeter of Triangle</returns>
        public override double Perimeter()
        {
            return baseValue + sideOne + sideTwo;
        }
    }
    class Call
    {
        public static void Main(String[] ar)
        {
            while (true)
            {
                Console.WriteLine("Select option");
                Console.WriteLine("1.Square");
                Console.WriteLine("2.Circle");
                Console.WriteLine("3.Rectangle");
                Console.WriteLine("4.Triangle");
                Console.WriteLine("5.Exit");
                int opt = Convert.ToInt32(Console.ReadLine());
                switch (opt)
                {
                    case 1:
                        Console.WriteLine("Enter side value for square");
                        float side = Convert.ToSingle(Console.ReadLine());
                        Square square = new Square(side);
                        Console.WriteLine("Area of Square " + String.Format("{0:0.00}", square.Area()));
                        Console.WriteLine("Perimeter of Square " + String.Format("{0:0.00}", square.Perimeter()));
                        break;
                    case 2:
                        Console.WriteLine("Enter radius value for circle");
                        float radius = Convert.ToSingle(Console.ReadLine());
                        Circle circle = new Circle(radius);
                        Console.WriteLine("Area of circle " + String.Format("{0:0.00}", circle.Area()));
                        Console.WriteLine("Perimeter of circle " + String.Format("{0:0.00}", circle.Perimeter()));
                        break;
                    case 3:
                        Console.WriteLine("Enter Length value for Rectangle");
                        float length = Convert.ToSingle(Console.ReadLine());
                        Console.WriteLine("Enter width value for Rectangle");
                        float width = Convert.ToSingle(Console.ReadLine());
                        Rectangle rect = new Rectangle(length, width);
                        Console.WriteLine("Area of Rectangle " + String.Format("{0:0.00}", rect.Area()));
                        Console.WriteLine("Perimeter of Rectangle " + String.Format("{0:0.00}", rect.Perimeter()));
                        break;
                    case 4:
                        Console.WriteLine("Enter base value for Triangle");
                        float basevalue = Convert.ToSingle(Console.ReadLine());
                        Console.WriteLine("Enter height value for Triangle");
                        float height = Convert.ToSingle(Console.ReadLine());
                        Console.WriteLine("Enter side value(a) for Triangle");
                        float side1 = Convert.ToSingle(Console.ReadLine());
                        Console.WriteLine("Enter side value(b) for Triangle");
                        float side2 = Convert.ToSingle(Console.ReadLine());
                        Triangle triangle = new Triangle(basevalue, height, side1, side2);
                        Console.WriteLine("Area of Triangle " + String.Format("{0:0.00}", triangle.Area()));
                        Console.WriteLine("Perimeter of Triangle " + String.Format("{0:0.00}", triangle.Perimeter()));
                        break;
                    default:
                        Environment.Exit(0);
                        break;
                }
                Console.WriteLine();
            }
        }
    }
}
